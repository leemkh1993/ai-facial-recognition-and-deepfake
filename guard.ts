import { Request, Response, NextFunction } from 'express';


export const checkLoginAPI = (req: Request, res: Response, next: NextFunction) => {
    if (req.session && req.session.username){
        console.log('Login Check Passed')
        next();
    } else {
        res.status(401).json({access: false})
    }
};