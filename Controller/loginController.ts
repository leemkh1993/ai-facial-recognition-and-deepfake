import express, { Request, Response } from 'express';
import { LoginService } from '../Service/loginService';
import { checkPW } from '../hash';
import expressSession from 'express-session';

export class LoginController {
    constructor(
        private service: LoginService,
    ) {}

    post = async (req: Request, res: Response) => {
        try {
            const username: string = req.body.username;
            const password: string = req.body.password;
            const getUserInfo = await this.service.getUserInfo(username);
            if (getUserInfo[0]) {
                console.log(getUserInfo[0]["password"]);
                const match = await checkPW(password, getUserInfo[0]["password"]);
                if (match) {
                    console.log(req.session);
                    if (req.session) {
                        req.session.username = username;
                        const firstName = getUserInfo[0]['first_name'];
                        const lastName = getUserInfo[0]['last_name'];
                        res.status(200).json({ Success: true, firstName: firstName, lastName: lastName });
                    };
                } else {
                    res.status(401).json({ CorrectInfo: false });
                };
            } else {
                res.status(404).json({ UserFound: false });
            };
        } catch (err) {
            console.log(err.message);
            res.status(500).json(err.message);
        };
    };

    put = async (req: Request, res: Response) => {
        try {
            const username = req.body.username;
            const password = req.body.password;
            const getUserInfo: any = await this.service.getUserInfo(username);
            if (getUserInfo[0]) {
                await this.service.createUser(username, password, 'test', 'test')
                if (req.session) {
                    req.session.username = username;
                    res.status(200).json({ Success: true });
                };
            } else {
                res.status(404).json({ UserFound: true });
            };
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ ServerError: true });
        };
    };

    get = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                console.log("hihihi")
                req.session.destroy(function (error) {
                    if (error) {
                        console.log(error);
                    }
                    console.log('Logout');
                    res.redirect('/');
                });
            } else {
                console.log('Logout btn has been click: no req.session')
                res.redirect('/');
            };
        } catch (err) {
            console.log(err.message)
            res.status(500).json(err)
        }
    };
}