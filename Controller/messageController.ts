import {Request, Response} from 'express';
import { MessageService } from '../Service/messageService'
import Knex from 'knex';

export class MessageController {
    constructor(
        private service: MessageService,
        private server: SocketIO.Server
    ){}

    get = async (req: Request, res: Response) => {
        try {
            const getMessages: Promise<Knex.QueryBuilder<any[]>>[] = await this.service.getMessage();
            res.status(200).json(getMessages)
        } catch (err) {
            console.log(err.message)
            res.status(500).json({'access': false})
        }
    }

    post = async (req: Request, res: Response) => {
        try {
            // Get content
            if (req.session) {
                // Check if content != empty
                const user:string = req.session.username;
                if (user) {
                    const user_id = await this.service.getUserInfo(user)
                    const newComment: string = req.body.content;
                    console.log(newComment)
                    const createMessage = await this.service.createMessage(user_id[0].id, newComment);
                    this.server.emit('newMessage', user, newComment, createMessage[0])
                    res.status(200).json({'created': true})
                } else {
                    res.status(401).json({'content': false})
                }
            } else {
                res.status(401).json({signIn: false})
            }
        } catch (err) {
            console.log(err.message)
            res.status(500).json({'created': false})
        }
    }

    put = async (req: Request, res: Response) => {
        try {
            const id = req.body.id;
            const newContent = req.body.newContent;
            if (id && newContent) {
                await this.service.updateMessage(id, newContent)
                this.server.emit('updateMessage', id, newContent)
                res.status(200).json({'updated': true})
            } else {
                res.status(401).json({'id/content': false})
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).json({'updated': false})
        }
    }

    delete = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const user = req.session.username;
                if (user) {
                    const id: number = parseInt(req.params.id);
                    const msg = await this.service.getMessage(user, id);
                    if (msg[0]) {
                        await this.service.deleteMessage(id);
                        this.server.emit('deleteMessage', id)
                        res.status(200).json({deleted: true})
                    } else {
                        res.status(401).json({unauthorised: true})
                    }
                } else {
                    res.status(401).json({login: false})
                }
            }
            res.status(200).json({ deleted: true })
        } catch (err) {
            console.log(err.message)
            res.status(500).json({'deteled': false})
        }
    }
}
