import Knex from "knex";
import configs from '../knexfile';
import { Request, Response } from 'express';
import { PhotoService } from '../Service/photoService';
import { PhotoController } from './photoController';

jest.mock('express')

describe('Photo Controller test case', () => {
    let photoService: PhotoService;
    let photoController: PhotoController;
    let emit: jest.Mock;
    let io: SocketIO.Server;
    let req: Request;
    let res: Response;
    let resJson: jest.SpyInstance;
    let resStatus: jest.SpyInstance;

    beforeAll(function () {
        // Create dummy service, pass in {} as Knex to simulate Knex object
        photoService = new PhotoService({} as Knex)
        // Spy on methods in class Message Service,
        // to simulate the return from each method

        jest.spyOn(photoService, 'getPhotos').mockImplementation(() => 
            Promise.resolve([{id: 1, user_id: 1, photo_name: 'test.jpg'}])
        );

        jest.spyOn(photoService, 'createPhoto').mockImplementation(() => 
            Promise.resolve([1])
        );

        jest.spyOn(photoService, 'deletePhoto').mockImplementation(() => 
            Promise.resolve(1)
        );
        io = ({
            emit: jest.fn((event, message, message2) => {null})
        } as any) as SocketIO.Server

        photoController = new PhotoController(photoService, io)
    })

    beforeEach(() => {
        req = ({
            body: {},
            session: {
                username: {}
            },
            params: {},
            file: {}
        } as any) as Request
        res = ({
            status: () => {return res},
            json: () => {}
        } as any) as Response
        resStatus = jest.spyOn(res, 'status')
        resJson = jest.spyOn(res, 'json')
    });

    it('GET method SUCCESS', async () => {
        //@ts-ignore
        req.session.username = 'test';
        await photoController.get(req, res)
        expect(photoService.getPhotos).toBeCalledTimes(1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith([{id: 1, user_id: 1, photo_name: 'test.jpg'}])
    });
    
    it('POST method SUCCESS', async () => {
        req.body = {
            content: 'content'
        }
        //@ts-ignore
        req.session = {
            username: 'test'
        }
        req.file = {
            filename: 'test.jpg'
        } as any 
        await photoController.post(req, res);
        expect(photoService.createPhoto).toBeCalledTimes(1);
        expect(io.emit).toBeCalledTimes(1)
        expect(io.emit).toBeCalledWith('newPhoto', 'test.jpg', 1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({ uploaded: true })
    });

    it('DEL method SUCCESS', async () => {
        //@ts-ignore
        req.params.id = '1'
        await photoController.delete(req, res);
        expect(photoService.deletePhoto).toBeCalledTimes(1);
        expect(io.emit).toBeCalledTimes(1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({'deleted': true})
    });
})