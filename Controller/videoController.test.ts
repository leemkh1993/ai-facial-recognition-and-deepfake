import Knex from 'knex';
import { VideoController } from "./videoController";
import { VideoService } from '../Service/videoService';
import { Request, Response } from 'express';

describe('Video Controller Test', () => {
    let videoService: VideoService;
    let videoController: VideoController;
    let emit: jest.Mock;
    let io: SocketIO.Server;
    let req: Request;
    let res: Response;
    let resStatus: jest.SpyInstance;
    let resJson: jest.SpyInstance;

    beforeAll(async () => {
        videoService = new VideoService({} as Knex);

        jest.spyOn(videoService, 'getVideos').mockImplementation(() =>
            Promise.resolve([{id: 1, user_id: 1, video_name: 'test.mp4'}])
        );

        jest.spyOn(videoService, 'createVideos').mockImplementation(() => 
            Promise.resolve([1])
        );

        jest.spyOn(videoService, 'deleteVideos').mockImplementation(() => 
            Promise.resolve(1)
        );

        io = ({
            emit: jest.fn((getVideoName, id) => {null})
        } as any) as SocketIO.Server;

        videoController = new VideoController(videoService, io);
    });

    beforeEach(async () => {
        req = ({
            session: {
                username: {}
            },
            file: {
                filename: {}
            },
            params: {
                id: {}
            }
        } as any) as Request;

        res = ({
            status: () => {return res},
            json: () => {}
        } as any) as Response;

        resStatus = jest.spyOn(res, 'status');
        resJson = jest.spyOn(res, 'json');
    });

    it('GET method SUCCESS', async () => {
        //@ts-ignore
        req.session.username = 'test'
        await videoController.get(req, res);
        expect(videoService.getVideos).toBeCalledTimes(1);
        expect(videoService.getVideos).toBeCalledWith('test');
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith([{id: 1, user_id: 1, video_name: 'test.mp4'}])
    });

    it('POST method SUCCESS', async () => {
        //@ts-ignore
        req.session.username = 'test'
        req.file.filename = 'test.mp4'
        await videoController.post(req, res)
        expect(videoService.createVideos).toBeCalledTimes(1)
        expect(videoService.createVideos).toBeCalledWith('test', 'test.mp4')
        expect(io.emit).toBeCalledTimes(1)
        expect(io.emit).toBeCalledWith('newVideo','test.mp4', 1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({ uploaded: true })
    });

    it('DEL method SUCCESS', async () => {
        req.params.id = '1'
        await videoController.delete(req, res)
        expect(videoService.deleteVideos).toBeCalledWith(1)
        expect(videoService.deleteVideos).toBeCalledTimes(1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({ deleted: true })
    });
});