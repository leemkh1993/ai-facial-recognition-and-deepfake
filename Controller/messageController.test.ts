import Knex from "knex";
import configs from '../knexfile';
import { MessageService } from '../Service/messageService';
import { MessageController } from './messageController';
import { Request, Response } from 'express';

jest.mock('express')

describe('Message Controller test case', () => {
    let messageService: MessageService;
    let messageController: MessageController;
    let emit: jest.Mock;
    let io: SocketIO.Server;
    let req: Request;
    let res: Response;
    let resJson: jest.SpyInstance;
    let resStatus: jest.SpyInstance;

    beforeAll(function () {
        // Create dummy service, pass in {} as Knex to simulate Knex object
        messageService = new MessageService({} as Knex)
        // Spy on methods in class Message Service,
        // to simulate the return from each method
        jest.spyOn(messageService, 'getUserInfo').mockImplementation(() =>
            Promise.resolve([{id: 1, username: 'test', password: '123'}])
        );

        jest.spyOn(messageService, 'getMessage').mockImplementation(() => 
            Promise.resolve([{id: 1, user_id: 1, content: 'Hello World'}])
        );

        jest.spyOn(messageService, 'createMessage').mockImplementation(() => 
            Promise.resolve([1])
        );

        jest.spyOn(messageService, 'updateMessage').mockImplementation(() => 
            Promise.resolve(['Hello World'])
        );

        jest.spyOn(messageService, 'deleteMessage').mockImplementation(() => 
            Promise.resolve(1)
        );
        io = ({
            emit: jest.fn((event, message, message2) => {null})
        } as any) as SocketIO.Server

        messageController = new MessageController(messageService, io)
    })

    beforeEach(() => {
        req = ({
            body: {},
            session: {
                username: {}
            },
            params: {}
        } as any) as Request
        res = ({
            status: () => {return res},
            json: () => {}
        } as any) as Response
        resStatus = jest.spyOn(res, 'status')
        resJson = jest.spyOn(res, 'json')
    });

    it('GET method SUCCESS', async () => {
        await messageController.get(req, res)
        expect(messageService.getMessage).toBeCalledTimes(1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith([{id: 1, user_id: 1, content: 'Hello World'}])
    });
    
    it('POST method SUCCESS', async () => {
        req.body = {
            content: 'content'
        }
        //@ts-ignore
        req.session = {
            username: 'test'
        }
        await messageController.post(req, res);
        expect(messageService.getUserInfo).toBeCalledTimes(1);
        expect(messageService.createMessage).toBeCalledTimes(1);
        expect(io.emit).toBeCalledTimes(1)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({'created': true})
    });

    it('PUT method SUCCESS', async () => {
        req.body = {
            id: 1,
            newContent: 'hihi'
        }
        await messageController.put(req, res);
        expect(messageService.updateMessage).toBeCalledTimes(1);
        expect(messageService.updateMessage).toBeCalledWith(req.body.id, req.body.newContent);
        expect(io.emit).toBeCalledTimes(2)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({'updated': true})
    });

    it('DEL method SUCCESS', async () => {
        //@ts-ignore
        req.session.username = 'test'
        req.params.id = '1'
        await messageController.delete(req, res);
        expect(messageService.getMessage).toBeCalledTimes(2);
        //@ts-ignore
        expect(messageService.getMessage).toBeCalledWith(req.session.username, parseInt(req.params.id));
        expect(messageService.deleteMessage).toBeCalledTimes(1);
        expect(io.emit).toBeCalledTimes(3)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({'deleted': true})
    });
})