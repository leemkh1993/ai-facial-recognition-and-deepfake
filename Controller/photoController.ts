import { Request, Response } from 'express';
import { PhotoService } from '../Service/photoService'

export class PhotoController {
    constructor(
        private service: PhotoService,
        private io: SocketIO.Server
    ) { }

    get = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const user: string = req.session.username;
                const getPhotos = await this.service.getPhotos(user)
                res.status(200).json(getPhotos)
            } else {
                res.status(401).json({access: false})
            } 
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ access: false })
        }
    };

    post = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const user: string = req.session.username;
                const getPhotoName: string = req.file.filename
                if (getPhotoName) {
                    const id = await this.service.createPhoto(user, getPhotoName)
                    this.io.emit('newPhoto', getPhotoName, id[0])
                    res.status(200).json({ uploaded: true })
                } else {
                    res.status(412).json({ uploaded: false })
                }
            } res.status(401).json({ access: false })
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ ServerError: true })
        }
    };

    delete = async (req: Request, res: Response) => {
        try {
            const id: number = parseInt(req.params.id);
            await this.service.deletePhoto(id)
            res.status(200).json({ deleted: true })
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ 'deleted': false })
        }
    };
}