import { LoginController } from './loginController';
import { LoginService } from '../Service/loginService';
import { Request, Response } from 'express';
import Knex from 'knex';

jest.mock('express')

describe('Login Controller test case', () => {
    let loginService: LoginService;
    let loginController: LoginController;
    let resJson: jest.SpyInstance;
    let resStatus: jest.SpyInstance;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        // Setup Service
        loginService = new LoginService({} as Knex);
        // Create Spy to simulate data being fed in from DB
        jest.spyOn(loginService, "getUserInfo").mockImplementation((username) => 
            Promise.resolve([{id: 1, username: 'test', password: '$2a$10$JOq43j94GRXOV7aQFBDJ0ucV1eCd6bGi4T6Ptviv1hbj4sJPWZH6a'}]));

        // Create Spy to simulate data being fed in from DB
        jest.spyOn(loginService, "createUser").mockImplementation((username, password) => 
            Promise.resolve([1])
        );

        // Setup Controller
        loginController = new LoginController(loginService)

        // Setup req to simulate what info are expected from frontend in real case
        req = ({
            body: {},
            session: {}
        } as any) as Request;
        // Setup res to simulate what info are expected to send back to frontend in real case
        res = ({
            status: () => {return res},
            json: () => {}
        } as any) as Response;
        // Spy so later can be tested in test case
        resStatus = jest.spyOn(res, 'status');
        resJson = jest.spyOn(res, 'json');
    })

    it('testing GET method SUCCESS', async () => {
        req.body = {
            username: 'test',
            password: '123'
        }
        await loginController.post(req, res)
        expect(loginService.getUserInfo).toBeCalledTimes(1)
        expect(loginService.getUserInfo).toBeCalledWith(req.body.username)
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({Success: true})
    })

    it('Testing post method SUCCESS', async () => {
        req.body = {
            username: 'test',
            password: '123'
        }
        await loginController.put(req, res)
        expect(loginService.getUserInfo).toBeCalledTimes(1)
        expect(loginService.getUserInfo).toBeCalledWith(req.body.username)
        expect(loginService.createUser).toBeCalledTimes(1)
        expect(loginService.createUser).toBeCalledWith(req.body.username, req.body.password, 'test', 'test')
        expect(resStatus).toBeCalledWith(200)
        expect(resJson).toBeCalledWith({Success: true})
    })
});