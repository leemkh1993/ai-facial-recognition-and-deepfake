import { Request, Response } from 'express';
import { VideoService } from '../Service/videoService'

export class VideoController {
    constructor(
        private service: VideoService,
        private io: SocketIO.Server
    ) { }

    get = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const user: string = req.session.username;
                const getVideos = await this.service.getVideos(user)
                res.status(200).json(getVideos)
            } else {
                res.status(401).json({ access: false })
            }
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ 'access': false })
        };
    };

    post = async (req: Request, res: Response) => {
        try {
            if (req.session) {
                const user: string = req.session.username;
                const getVideoName: string = req.file.filename
                if (getVideoName) {
                    const id = await this.service.createVideos(user, getVideoName)
                    this.io.emit('newVideo', getVideoName, id[0])
                    res.status(200).json({ uploaded: true })
                } else {
                    res.status(412).json({ uploaded: false })
                }
            } res.status(401).json({ access: false })
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ ServerError: true })
        }
    };

    delete = async (req: Request, res: Response) => {
        try {
            const id: number = parseInt(req.params.id);
            await this.service.deleteVideos(id)
            res.status(200).json({ deleted: true })
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ 'deleted': false })
        };
    };
}