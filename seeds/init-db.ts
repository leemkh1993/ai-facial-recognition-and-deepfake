import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex.raw('truncate table users cascade');
    await knex.raw('truncate table comments cascade');
    await knex.raw('truncate table photos cascade');
    await knex.raw('truncate table videos cascade');

    // Inserts seed entries
    await knex('users').insert([
        // password = 123
        { username : 'marco', password: '$2a$10$JOq43j94GRXOV7aQFBDJ0ucV1eCd6bGi4T6Ptviv1hbj4sJPWZH6a', first_name : 'Marco', last_name: 'Lee'},
        { username : 'charlie', password: '$2a$10$JOq43j94GRXOV7aQFBDJ0ucV1eCd6bGi4T6Ptviv1hbj4sJPWZH6a', first_name : 'Charlie', last_name: 'Kwong'},
        { username : 'leo', password: '$2a$10$JOq43j94GRXOV7aQFBDJ0ucV1eCd6bGi4T6Ptviv1hbj4sJPWZH6a', first_name : 'Leo', last_name: 'Choi'}
    ]);

    await knex("comments").insert([
        { user_id: 1,content: "Hello World" },
        { user_id: 1,content: "Hello World" },
        { user_id: 1,content: "Hello World" },
        { user_id: 2,content: "Hello World" },
        { user_id: 2,content: "Hello World" },
        { user_id: 2,content: "Hello World" },
        { user_id: 3,content: "Hello World" },
        { user_id: 3,content: "Hello World" },
        { user_id: 3,content: "Hello World" }
    ]);

    await knex("photos").insert([
        { user_id: 1, photo_name: "test_photo.jpg" },
        { user_id: 1, photo_name: "test_photo.jpg" },
        { user_id: 1, photo_name: "test_photo.jpg" },
        { user_id: 2, photo_name: "test_photo.jpg" },
        { user_id: 2, photo_name: "test_photo.jpg" },
        { user_id: 2, photo_name: "test_photo.jpg" },
        { user_id: 3, photo_name: "test_photo.jpg" },
        { user_id: 3, photo_name: "test_photo.jpg" },
        { user_id: 3, photo_name: "test_photo.jpg" }
    ]);

    await knex("videos").insert([
        { user_id: 1, video_name: "test_video.mp4" },
        { user_id: 1, video_name: "test_video.mp4" },
        { user_id: 1, video_name: "test_video.mp4" },
        { user_id: 2, video_name: "test_video.mp4" },
        { user_id: 2, video_name: "test_video.mp4" },
        { user_id: 2, video_name: "test_video.mp4" },
        { user_id: 3, video_name: "test_video.mp4" },
        { user_id: 3, video_name: "test_video.mp4" },
        { user_id: 3, video_name: "test_video.mp4" }
    ]);
};
