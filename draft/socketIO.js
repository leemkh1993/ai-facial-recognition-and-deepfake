// //   Socket.io
// const socket = io.connect();

// const messageForm = document.querySelector('#create-messages-form');//input area in submittion
// const newMessageDiv = document.querySelector('#new-messages-container'); //in order to deliver new comment to div that newly create
// // const chatForm = document.getElementById('chat-form'); //text areas
// // const chatMessages = document.querySelector('.chat-messages');//my newMessageDiv 
// const roomName = document.getElementById('room-name');
// const userList = document.getElementById('users');

// // Get username and room from URL
// const { username, room } = Qs.parse(location.search, {
//     ignoreQueryPrefix: true
//   });
  
//   const socket = io();
  
//   // Join chatroom
//   socket.emit('joinRoom', { username, room });
  
//   // Get room and users
//   socket.on('roomUsers', ({ room, users }) => {
//     outputRoomName(room);
//     outputUsers(users);
//   });

//     // Message from the server 
//     socket.on('message', message => {
//     console.log(`${message} Socket.io service is connected!`)
//     outputMessage(message);

//   // Scroll down
//     newMessageDiv.scrollTop = newMessageDiv.scrollHeight;
//     });

//     // Message submit
//     messageForm.addEventListener('submit', e => {
//     e.preventDefault();
  
//     // Get message text
//     const msg = e.target.elements.msg.value; 
//     //id="input-text" msg from the text input
  
//     // Emit message to server
//     socket.emit('newMessageDiv', msg);
  
//     // Clear input
//     e.target.elements.msg.value = ''; // to clear message that input in the textarea 
//     e.target.elements.msg.focus(); // input area becomes empty
//     });

// // Output message to DOM
// function outputMessage(message) {
//     const div = document.createElement('div');
//     div.classList.add('message');
//     div.innerHTML = `
//     <div class="user-icon"><svg class="svg-inline--fa fa-user fa-w-14 fa-2x" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path></svg></div>
//     <div class="status">
//         <span class="cm-areas">${message.username} ${message.time} min ago
//         <p class="cm-text">${message.text}</p></span>
//     </div>
//     `
//     newMessageDiv.appendChild(div);
//     }

//     // Add room name to DOM
//     function outputRoomName(room) {
//         roomName.innerText = room;
//     }
    
//     // Add users to DOM
//     function outputUsers(users) {
//         userList.innerHTML = `
//         ${users.map(user => `<li>${user.username}</li>`).join('')}
//         `;
//     }



// // // function updatedMessage(message){
// // //   const div = document.createElement('div');
// // //   div.classList.add('postMessage');
// // //   div.innerHTML = `<p>${message}</p>`;
// // //   document.querySelector('#newMessagesContainer').appendChild(div);
// // // }

// // // New message from server

// // socket.on('newMessage', function (newMessage) {
  
// // });

// // // Update existing message
// // socket.on('updateMessage', function (id, newMessage) {
  
// // });

// // // New photo from server
// // socket.on('newPhoto', function (name, id) {
    
// // });

// // // New video from server
// // socket.on('newVideo', function (name, id){

// // });
