import Knex from "knex";
import { LoginService } from './loginService';
import configs from '../knexfile';
import { isNumber } from 'util';

describe('Login Service test case', () => {
    let loginService: LoginService;
    let knex: Knex;

    beforeAll(async () => {
        knex = Knex(configs.test)
        loginService = new LoginService(knex)
    })

    it('Test getUserInfo SUCCESS', async () => {
        const result: any[] = await loginService.getUserInfo('marco')
        expect(result[0].username).toBe('marco')
        expect(result[0].password).toBe('$2a$10$JOq43j94GRXOV7aQFBDJ0ucV1eCd6bGi4T6Ptviv1hbj4sJPWZH6a')
    })

    it('Test createUser SUCCESS', async () => {
        const result = await loginService.createUser('test', '123','test','test')
        expect(result).toHaveLength(1)
        expect(isNumber(result[0])).toBeTruthy()
    })

    afterAll(async () => {
        await knex.destroy()
    });
})