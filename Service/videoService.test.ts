import Knex from "knex"
import configs from '../knexfile';
import { VideoService } from './videoService';

describe('Video Service test case', () => {
    let knex: Knex;
    let videoService: VideoService;

    beforeAll (() => {
        knex = Knex(configs.test)
        videoService = new VideoService(knex)
    })

    it('getVideos Test SUCCESS', async () => {
        const user = 'marco'
        const result = await videoService.getVideos(user)
        const last = result.length - 1
        expect(result[0]).toHaveProperty('user_id')
        expect(result[last]).toHaveProperty('video_name')
    })

    it('createVideos Test SUCCESS', async () => {
        const createVideo = await videoService.createVideos('marco', 'test2.mp4')
        expect(createVideo.length).toBe(1)
    })

    it('deleteVideos Test SUCCESS', async () => {
        const insert = await videoService.createVideos('marco', 'test2.mp4')
        const deleted = await videoService.deleteVideos(insert[0])
        expect(deleted).toBe(1)
    })

    afterAll(() => {
        knex.destroy();
    })
})