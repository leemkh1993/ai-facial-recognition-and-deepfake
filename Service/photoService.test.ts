import Knex from "knex"
import configs from '../knexfile';
import { PhotoService } from './photoService';

describe('Photo Service test case', () => {
    let knex: Knex;
    let photoService: PhotoService;

    beforeAll (() => {
        knex = Knex(configs.test)
        photoService = new PhotoService(knex)
    })

    it('getPhotos Test SUCCESS', async () => {
        const user = 'marco'
        const result = await photoService.getPhotos(user)
        const last = result.length - 1
        expect(result[0]).toHaveProperty('user_id')
        expect(result[last]).toHaveProperty('photo_name')
    })

    it('createPhotos Test SUCCESS', async () => {
        const createPhotos = await photoService.createPhoto('marco', 'test2.jpg')
        expect(createPhotos.length).toBe(1)
    })

    it('deletePhoto Test SUCCESS', async () => {
        const insert = await photoService.createPhoto('marco', 'test.jpg')
        const deleted = await photoService.deletePhoto(insert[0])
        expect(deleted).toBe(1)
    })

    afterAll(() => {
        knex.destroy();
    })
})