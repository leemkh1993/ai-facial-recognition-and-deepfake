import Knex from "knex"
import configs from '../knexfile';
import { MessageService } from './messageService';
import { isDate } from 'util';

describe('Message test case', () => {
    let knex: Knex;
    let messageService: MessageService;

    beforeAll(async () => {
        knex = Knex(configs.test);
        messageService = new MessageService(knex);
    })

    it('getMessage SUCCESS', async () => {
        const result = await messageService.getMessage()
        const last = result.length - 1
        expect(result[0]).toHaveProperty('user_id')
        expect(result[last]).toHaveProperty('content')
    });

    it('createMessage SUCCESS', async () => {
        const newMessage = await messageService.createMessage(1, 'hello world');
        expect(newMessage.length).toBe(1)
    });

    it('updateMessage SUCCESS', async () => {
        const newMessage: any = await messageService.createMessage(1, 'hello world');
        const result = await messageService.updateMessage(newMessage[0], 'Bye World');
        expect(result[0]).toBe('Bye World')
    });

    it('deleteMessage SUCCESS', async () => {
        const newMessage: any = await messageService.createMessage(1, 'hello world');
        const result = await messageService.deleteMessage(newMessage[0]);
        expect(result).toBe(1)
    });

    afterAll(() => {
        knex.destroy();
    })
})