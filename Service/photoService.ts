import Knex from 'knex';

export class PhotoService {
    constructor(private knex: Knex){}

    async getPhotos(user: string) {
        const getID = await this.knex('users').select('id').where('username', user);
        return await this.knex.select('*').from('photos').where('user_id', getID[0].id);
    };

    async createPhoto(user: string, name: string) {
        const getID = await this.knex('users').select('id').where('username', user);
        return await this.knex('photos').insert({
            user_id: getID[0].id,
            photo_name: name
        }).returning('id');
    };

    async deletePhoto(id: number){
        return await this.knex('photos').where('id', id).del();
    };
}