import Knex from 'knex';

export class MessageService {
    constructor(private knex: Knex) { }

    async getUserInfo(user: string) {
        return this.knex('users').select('*').where('username', user)
    }

    async getMessage(user?: string, msgID?: number) {
        if (user && msgID) {
            const user_id = await this.knex('users').select('*').where('username', user)
            return await this.knex.select('*').from('comments').where('user_id', user_id[0].id).andWhere('id', msgID)
        } else {
            return await this.knex.select('*').from('comments');
        }
    }

    async createMessage(user_id: number, content: string) {
        return await this.knex('comments').insert({
            'user_id': user_id,
            'content': content,
        }).returning('id')
    }

    async updateMessage(id: number, content: any) {
        return await this.knex('comments').update({
            'content': content,
            'updated_at': this.knex.fn.now()
        }).where('id', id).returning('content');
    }

    async deleteMessage(id: number) {
        return await this.knex('comments').where('id', id).del();
    }
}