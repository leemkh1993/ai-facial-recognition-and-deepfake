import Knex from 'knex';
import {hashPW} from '../hash';

export class LoginService {
    constructor(
        private knex: Knex
    ){}

    getUserInfo = async (username: string) => {
        return await this.knex.select('*').from('users').where('username', username)
    }

    createUser = async (username: string, password: string, firstName: string, lastName: string) => {
        const hash = await hashPW(password)
        return await this.knex('users').insert({'username': username,
                                                'password': hash, 
                                                'first_name': firstName,
                                                'last_name': lastName}).returning('id')
    }
}