import Knex from 'knex';

export class VideoService {
    constructor(private knex: Knex){}

    async getVideos(user: string) {
        const getID = await this.knex('users').select('id').where('username', user)
        return await this.knex('videos').select('*').where('user_id', getID[0].id)
    }

    async createVideos(user: string, name: string){
        const getID = await this.knex('users').select('id').where('username', user)
        return await this.knex('videos').insert({
            user_id: getID[0].id,
            video_name: name
        }).returning('id')
    }

    async deleteVideos(id: number){
        return await this.knex('videos').where('id', id).del()
    }
}