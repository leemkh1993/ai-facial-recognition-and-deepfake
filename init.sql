CREATE DATABASE deepmedia;

CREATE TABLE users (
    user_id SERIAL PRIMARY KEY,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    created TIMESTAMP,
    updated TIMESTAMP
);

create table comments (
    id SERIAL PRIMARY KEY,
    content varchar(255) NOT NULL,
    created timestamp,
    updated timestamp,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES "user"(id)
);

create table photos (
    id SERIAL PRIMARY KEY,
    photo_name text,
    created TIMESTAMP,
    updated TIMESTAMP,
	user_id integer,
    FOREIGN KEY (user_id) REFERENCES "user"(id)
);

create table videos(
    id SERIAL PRIMARY KEY,
    video_name text,
    created TIMESTAMP,
    updated TIMESTAMP,
	user_id integer,
    FOREIGN KEY (user_id) REFERENCES "user"(id)
);

