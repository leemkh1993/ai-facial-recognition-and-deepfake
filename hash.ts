import * as bcryptjs from "bcryptjs";

const salt = 10;

export async function hashPW(pw: string) {
    const hash = await bcryptjs.hash(pw, salt);
    return hash;
};

export async function checkPW(pw: string, hashPw: string) {
    const match = await bcryptjs.compare(pw, hashPw);
    return match;
};
