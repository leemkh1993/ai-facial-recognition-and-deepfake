import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('users', table => {
        table.increments();
        table.string('username').notNullable();
        table.string('password').notNullable();
        table.string('first_name').notNullable();
        table.string('last_name').notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable('comments', table => {
        table.increments();
        table.integer('user_id').notNullable();
        table.foreign('user_id').references('users.id')
        table.string('content').notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable('photos', table => {
        table.increments();
        table.integer('user_id').notNullable();
        table.foreign('user_id').references('users.id')
        table.string('photo_name').notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable('videos', table => {
        table.increments();
        table.integer('user_id').notNullable();
        table.foreign('user_id').references('users.id')
        table.string('video_name').notNullable();
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.raw('drop table users cascade');
    await knex.raw('drop table comments cascade');
    await knex.raw('drop table photos cascade');
    await knex.raw('drop table videos cascade');
};

