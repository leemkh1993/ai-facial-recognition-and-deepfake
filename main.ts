import express, {Request, Response} from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import socketIO from 'socket.io';
import http from 'http';
import Knex from 'knex';
import configs from './knexfile';
import multer from 'multer';
// import jsonfile from 'jsonfile'
import expressSession from 'express-session';
import { LoginService } from './Service/loginService';
import { LoginController } from './Controller/loginController';
import { MessageService } from './Service/messageService';
import { MessageController } from './Controller/messageController';
import { PhotoService } from './Service/photoService';
import { PhotoController } from './Controller/photoController';
import { VideoService } from './Service/videoService';
import { VideoController } from './Controller/videoController';
// import util from 'util'
// import moment from 'moment';

// import { formatMessage } from './utils/messages.js';
// import { getCurrentUser,userJoin,userLeave, getRoomUsers } from './utils/users.js';
// import { getCurrentUser,userJoin,userLeave } from './utils/users.js';

const app = express();
const server = new http.Server(app);
const io = socketIO(server)

const sessionMiddleware = expressSession({
    secret: 'There is no secret',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: 'auto' }
})

app.use(sessionMiddleware);

io.use((socket, next) => {
    sessionMiddleware(socket.request, socket.request.res, next);
});

// Setup database
const knex = Knex(configs.development)

// Setup services and controllers
const loginService = new LoginService(knex)
export const loginController = new LoginController(loginService)
const messageService = new MessageService(knex)
export const messageController = new MessageController(messageService, io)
const photoService = new PhotoService(knex)
export const photoController = new PhotoController(photoService, io)
const videoService = new VideoService(knex)
export const videoController = new VideoController(videoService, io)

// Body Parser for receiving data from frontend
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Multer for uploading images and videos
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/Uploaded photo`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({ storage })


// Find something allowing video uploading

/* =================================Explain what this do======================================= */
// interface Capture{
//     image?:string
// }

// interface Video{
//     video?: string
// }

// const CAPTURES_JSON = path.join(__dirname,'captures.json');
// const VIDEOS_JSON = path.join(__dirname,'videoRecords.json');
// //action=capture
// app.post('/capture',upload.single('image'),async function(req,res){
//     try{
//     const capture:Capture[] = await jsonfile.readFile(CAPTURES_JSON);
//     console.log(req.file)    
//     capture.push({
//             image: req.file.filename
//         });
//         await jsonfile.writeFile(CAPTURES_JSON,capture,{spaces:1});
//         console.log(capture)
//         res.json(capture);        
//     }catch(e){
//         console.log(e.message)
//     }

//     });

// app.post('/video-recording',upload.single('video'),async function(req,res){
//     try{
//     console.log(req.body);
//     const video:Video[] = await jsonfile.readFile(VIDEOS_JSON);
//         video.push({
//             video: req.file.filename
//         });
//         await jsonfile.writeFile(VIDEOS_JSON,video,{spaces:1});
//         console.log(video)
//         res.json(video);        
//     }catch(e){
//         console.log(e.message)
//     }
//     });

/* =================================Explain what this do======================================= */

// Frontend accessing static files and routes
import { routes } from './routes';
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes)

// 所有middleware 都無response 就輪到佢
app.use('*',async function (req: Request, res: Response) {
    try {
        res.sendFile(path.join(__dirname, 'public/404.html'));
    } catch (err) {
        console.log(err.message)
    }
});

// Server listening to requests
const PORT = 8080;
server.listen(PORT, () => {
    console.log(`listening to port: ${PORT}`);
});

/* ===============================Explain what this do======================================= */
// const hostName = 'DeepMedia Host'
io.on('connection', async function (socket) {
    console.log(`Socket Id is ${socket.id}`)
    socket.request.session.socketIO = socket.id;
    socket.request.session.save();
    console.log(`${socket}: New WS Connection Welcome User!`)

    socket.on('woo', function(data){ //event, data
        console.log('woo');
        console.log(data)
    
    // Welcome current user
    socket.emit('Greeting', `'Welcome to Deep Media!${data}`)
    })
    

    // broadcast when a user connects
    // socket.broadcast.emit('message', 'A user has joined the chat!')

    // Runs when client disconnects
    // socket.on('disconnect', () => {
    //     io.emit('message', 'A user has left the chat');
    // });
})

/* =================================Explain what this do======================================= */
io.on('connection', (socket) => {
    console.log(socket.id)
    socket.emit('Welcome')
})


