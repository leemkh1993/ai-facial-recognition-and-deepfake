import express from 'express';
import { upload, loginController, messageController, photoController, videoController } from './main';
import { checkLoginAPI } from './guard';

export const routes = express.Router()

routes.post('/login', loginController.post)
routes.put('/register', loginController.put)
routes.get('/logout', loginController.get)

routes.get('/getMessage', messageController.get)
routes.post('/submitMessage', checkLoginAPI, messageController.post)
routes.put('/updateMessage/:id', checkLoginAPI, messageController.put)
routes.delete('/deleteMessage/:id', checkLoginAPI, messageController.delete)

routes.get('/getPhotos', checkLoginAPI, photoController.get)
routes.post('/submitPhotos/:id', checkLoginAPI, upload.single('image'), photoController.post)
routes.delete('/deletePhotos/:id', checkLoginAPI, photoController.delete)

routes.get('/getVideo', checkLoginAPI, videoController.get)
routes.post('/submitVideo/:id', checkLoginAPI, upload.single('video'), videoController.post)
routes.delete('/deleteVideo/:id', checkLoginAPI, videoController.delete)
