const video = document.getElementById('vid1');


// import cors from "cors";
// app.use(cors());


//Load 4 model for the detection
Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/models')
// ]).then(startVideo);
]).then(console.log("Trytry"));


// //link to video, i.e. getUserMedia
function startVideo() {
  navigator.mediaDevices.getUserMedia(
    { audio: true,
        // {
        // autoGainControl:true,
        // echoCancellation: true,
        // noiseSuppression:true
        // },
      video: true
      })
      .then(stream => {video.srcObject = stream})
      .catch(err => console.error(err));
};




// Action taken when the video above (i.e. getUserMedia) played
video.addEventListener('play', () => {

    console.log("emoji for video active");
    //initial the setting
    const canvas = faceapi.createCanvasFromMedia(video);
    document.body.append(canvas);
    const displaySize = { width: video.width, height: video.height };
    faceapi.matchDimensions(canvas, displaySize);
    document.querySelector("canvas").style.display = ("none");


    //Frequency for the detection
    setInterval(async () => {

      //Await faceapi for detection on face, face landmarks & face expression
      const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
      const resizedDetections = faceapi.resizeResults(detections, displaySize)
      //Draw the result on the canvas
      canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
      // faceapi.draw.drawDetections(canvas, resizedDetections)
      // faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
      // faceapi.draw.drawFaceExpressions(canvas, resizedDetections);


      // console.log the output of the detection
      // console.log(resizedDetections);
      // console.log(resizedDetections[0].expressions.happy);
      // console.log(typeof(resizedDetections[0].expressions.happy));

      // code for the emoji when facial expression detected
      if (resizedDetections[0]){// if you smile
        if (resizedDetections[0].expressions.happy > 0.87){
          console.log("you are happy");
          const overlay = document.createElement("img");
          overlay.src="http://i.stack.imgur.com/SBv4T.gif";
          overlay.alt="emoji overlay.";
          overlay.style.cssText =`
          position:absolute;
          left:${resizedDetections[0].alignedRect.box.left-500}px;
          top:${resizedDetections[0].alignedRect.box.top-230}px;
          width:${resizedDetections[0].alignedRect.box.width*3.29}px;
          transform:rotate(5px);
          `;
          overlay.id="happy";
          document.getElementById('facial-emoji-layer').appendChild(overlay);
        } else if//if you o嘴
            (resizedDetections[0].expressions.surprised > 0.87){
            console.log("you are surprised");
            const overlay = document.createElement("img");
            overlay.src="https://thumbs.gfycat.com/ImpressiveFixedGermanshepherd-size_restricted.gif";
            overlay.alt="emoji overlay.";
            overlay.style.cssText =`
            position:absolute;
            left:${resizedDetections[0].alignedRect.box.left-430}px;
            top:${resizedDetections[0].alignedRect.box.top-120}px;
            width:${resizedDetections[0].alignedRect.box.width*2.7}px;
            transform:rotate(5px);
            `;
            overlay.id="happy";
            document.getElementById('facial-emoji-layer').appendChild(overlay);
          } else{//delete the shown emoji if u 冇表情
              if(document.getElementById("happy")){
              const overlay2 = document.getElementById("happy");
              overlay2.parentNode.removeChild(overlay2);
            }
          }
      };

      //code for the emoji when speech detected
      

    }, 160);
  });

  
  