//let variables
let recognizer;
let words;
let wordList;
let modelLoaded = false;

//words the model can listen
$( document ).ready(function() {
    wordList = ["zero","one","two","three","four",
                "five","six","seven","eight","nine",
                "yes", "no", "up", "down", "left",
                 "right", "stop", "go"];
});


//  on/off btn
$("#audio-switch").change(function() {
    if(this.checked){
        if(!modelLoaded){
            loadModel();
        }else{
            startListening();
        }
    }   else {
        stopListening();
    }   
});


//loading the model
function loadModel(){

    $(".progress-bar").removeClass('d-none'); 
    //'BROWSER_FFT' <--- one of the type of input
    recognizer = speechCommands.create('BROWSER_FFT');//the second params is needed if use own model, now default 18w model it provided 

    Promise.all([
        recognizer.ensureModelLoaded()
      ]).then(function(){
        $(".progress-bar").addClass('d-none');
        words = recognizer.wordLabels();
        modelLoaded = true;
        startListening();
        console.log("ready for audio emoji");
      })
}


//
function startListening(){//`listen()` takes two arguments:

    // 1.Callback function that is invoked anytime a word is recognized.
    recognizer.listen(({scores}) => {
        // scores contains the probability scores that correspond to recognizer.wordLabels().
        // Turn scores into a list of (score,word) pairs.
        scores = Array.from(scores).map((s, i) => ({score: s, word: words[i]}));
        // Find the most probable word.
        scores.sort((s1, s2) => s2.score - s1.score);
        console.log(scores[0].word);
        
            if(scores[0].word=="right"){
                console.log("yeah");
                const overlaysp = document.createElement("img");
                overlaysp.src="https://i.gifer.com/GrBp.gif";
                overlaysp.alt="emoji overlay.";
                overlaysp.style.cssText =`
                position:absolute;
                left:50px;
                top:300px;
                width:300px;
                transform:rotate(5px);
                `;
                overlaysp.id="speech-emoji";
                document.getElementById('facial-emoji-layer').appendChild(overlaysp);
                // const overlay2 = document.getElementById("speech-emoji");
            } else if (scores[0].word=="yes"){
                console.log("yesh");
                const overlaysp = document.createElement("img");
                overlaysp.src="http://cdn.lowgif.com/full/ba38151de36eb27d-.gif";
                overlaysp.alt="emoji overlay.";
                overlaysp.style.cssText =`
                position:absolute;
                left:50px;
                top:300px;
                width:300px;
                transform:rotate(5px);
                `;
                overlaysp.id="speech-emoji";
                document.getElementById('facial-emoji-layer').appendChild(overlaysp);
                // const overlay2 = document.getElementById("speech-emoji");
            } else if (scores[0].word=="stop"){
                while (document.getElementById("speech-emoji")){
                    document.getElementById("speech-emoji").parentNode.removeChild(document.getElementById("speech-emoji")); 
                }
                while(document.getElementById("happy")){
                    document.getElementById("happy").parentNode.removeChild(document.getElementById("happy"));
                }
            } else{
                
            }
            // if(document.getElementById("speech-emoji")){
            //     setTimeout(
            //     document.getElementById("speech-emoji").parentNode.removeChild(document.getElementById("speech-emoji"))
            //     ,5000)
            // }
    }, 
    {
        // 2. Configuration object with adjustable fields
        probabilityThreshold: 0.75
    });
}

function stopListening(){
    recognizer.stopListening();
}