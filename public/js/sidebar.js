/************************* This is popover ************************/
$(function () {
  // Enables popover
  $("[data-toggle=popover]").popover();
});

function openNav() {
  document.querySelector("#openbtn").addEventListener('click', function () {
    document.querySelector("#mySidebar").style.width = "325px";
    document.querySelector("#main").style.marginLeft = "90px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.7)";
  })
}

function closeNav() {
  document.querySelector("#mySidebar").style.width = "0";
  document.querySelector("#main").style.marginLeft = "0px";
  document.querySelector("#capture").style.marginLeft = "0px";

};

function animateButton() {
  document.getElementById('capture').addEventListener('click', function () {
  document.querySelector('#openbtn').classList.add('blink_text');
  });
  document.getElementById('btnStop').addEventListener('click', function () {
  document.querySelector('#openbtn').classList.add('blink_text');
  });
}
animateButton();
/************************ Below is getUserMedia elements *******************/


let constraints = {
  audio: true,
  video: true
  // {facingMode: "environment"
    // width: { min: 640, ideal: 1280, max: 1920 },
    // height: { min: 480, ideal: 720, max: 1080 }
  // }
};
// facingMode: { exact: "user" }
// facingMode: "environment"

//handle older browsers that might implement getUserMedia 
if (navigator.mediaDevices === undefined) {
  navigator.mediaDevices = {};
  navigator.mediaDevices.getUserMedia = function (constraints) {
    let getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (!getUserMedia) {
      return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
    }
    return new Promise(function (resolve, reject) {
      getUserMedia.call(navigator, constraints, resolve, reject);
    });
  }
} else {
  navigator.mediaDevices.enumerateDevices()
    .then(devices => {
      devices.forEach(device => {
        console.log(device.kind.toUpperCase(), device.label);//device.Id
        //, device.deviceId
      })
    })
    .catch(err => {
      console.log(err.name, err.message);
    })
}

navigator.mediaDevices.getUserMedia(constraints)
  .then(function (mediaStreamObj) {
    //connect the media stream to the first video element
    // const video = document.querySelector('#video')
    const video = document.querySelector('video');
    if ("srcObject" in video) {
      video.srcObject = mediaStreamObj;
    } else {
      // old version
      video.src = window.URL.createObjectURL(mediaStreamObj);
    }
    video.onloadedmetadata = function () {
      //show in the video element what is being captured by the webcam
      video.play();
    };
    //add listeners for screenshop 
    // const video = document.querySelector("#videoElement")
    let canvas = document.querySelector('#canvas');
    let screencap = document.querySelector('#screencap');
    let captureButton = document.querySelector('#capture');
    let context = canvas.getContext('2d');

    captureButton.addEventListener('click', function () {
    let dataURL = canvas.toDataURL("image/png") //same as screencap.src
    context.drawImage(video, 0, 0, 255, 255);
    screencap.setAttribute('src', canvas.toDataURL('image/png'))
    console.log(`This is the image source ${dataURL}`)
    //e.g.data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XS...
   
    
    })

    //add listeners for multipleShot
    function multipleShot(){
    captureButton.addEventListener('click', function () {
    let newImage = document.createElement('img')
    let imageContainer = document.querySelector('.captures-container')
    imageElements = imageContainer.getElementsByTagName('img')
    let newImageSource = canvas.toDataURL('image/png')
    // let newImageSource = "/media/beach.png"

    newImage.setAttribute("class", "new")
    newImage.setAttribute("src", newImageSource);
    imageContainer.appendChild(newImage)        
    })
    }
    multipleShot();

    // });  
    //trigger the submit button Capture                
    /* capture store in the Server */

    //add listeners for saving video/audio
    let start = document.getElementById('btnStart');
    let stop = document.getElementById('btnStop');
    let vidSave = document.getElementById('vid2');
    let mediaRecorder = new MediaRecorder(mediaStreamObj);
    let chunks = [];

    start.addEventListener('click', function(){
        mediaRecorder.start();
        console.log(`click start is triggered ${mediaRecorder.state}`);
      })
      stop.addEventListener('click', function() {
        mediaRecorder.stop();
        console.log(`Media status: ${mediaRecorder.state}`);

      });
      mediaRecorder.ondataavailable = function(){
        chunks.push(event.data);
      }
      mediaRecorder.onstop = function(){
        let blob = new Blob(chunks, { 'type': 'video/mp4;' });
        // let blobUrl = URL.createObjectURL(blob) 
        //for store in the server 
        chunks = [];
        let videoURL = window.URL.createObjectURL(blob);
        // let vidSrcisString = vidSave.src //this is blobURL : "blob:http://localhost:8080/f632fa47-4dfa-432e-a672-78a726b1ba20"
        // let getvidURLSrc = ""
        vidSave.src = videoURL;
        console.log(`Media status: ${mediaRecorder.state}`);
        // getvidURLSrc = vidSrcisString.slice(5, ) //slice blobURL e.g."http://localhost:8080/f632fa47-4dfa-432e-a672-78a726b1ba20"
        // console.log(getvidURLSrc)
        console.log(`This is the video source ${vidSave.src}`)
        
      }
        //more videoRecordings
      //   function takeMoreRecordings(){
      //   let newVideoRecords = document.createElement('video')
      //   let videoContainer = document.querySelector('.video-recording-container')
      //   let newVideoSource = new Blob(chunks, { 'type': 'video/mp4;' });
      //   chunks = [];

      //   let videoURL = window.URL.createObjectURL(newVideoSource);

      //   start.addEventListener('click', function(){
      //   mediaRecorder.start();
      //   console.log(mediaRecorder.state);
      //   })
      //   stop.addEventListener('click', function() {
      //   mediaRecorder.stop();
      //   console.log(`Media status: ${mediaRecorder.state}`);
      //   });
      //   mediaRecorder.ondataavailable = function(){
      //   chunks.push(event.data);
      //   }

      //   mediaRecorder.onstop = function () {
      //   newVideoRecords.setAttribute("id", "vid3")
      //   newVideoRecords.setAttribute("src", newVideoSource)
      //   vidSave.src = videoURL;
      //   videoContainer.appendChild(newVideoRecords)
      //   }

      // }


})
  .catch(function (err) {
    console.log(err.name, err.message);
  });
