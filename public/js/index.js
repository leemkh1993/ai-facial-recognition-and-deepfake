/************************* This is popover ************************/
$(function () {
  // Enables popover
  $("[data-toggle=popover]").popover();
});

function createUser(){
let name = "Busy";
let lastname = "Bee";
let initials = name.charAt(0)+""+lastname.charAt(0);
document.getElementById("name").innerHTML = initials;  
}
createUser();

/************************* This is popover ************************/
function changeButtonText() {
  const showChatButton = document.querySelector('#show')
  // const hideChatButton = document.querySelector('#hide')
 
  showChatButton.addEventListener('click', function(){
    showChatButton.innerText = ""
    showChatButton.innerText += `HIDE CHAT`;//SHOW CHAT FUNCTION
  })
}
changeButtonText();

/* Start button to open the WebCam */
function openWebCam() {
  const startButton = document.querySelector('#startbutton')
  
  startButton.addEventListener('click', function () {
    startButton.innerHTML =" ";
    startButton.innerHTML +=`
    <div><i class="fa fa-circle text-danger"></i>
    <span>&nbsp; LIVE NOW</span>&nbsp
    <i class="fas fa-video" style="color:#000;"></i>
    </div>`;
  })
}

/* Close Button to end live streaming event */
function callEnds() {
  const startButton = document.querySelector('#startbutton')
  const closeWebCambutton = document.querySelector('#closebutton')
  const video = document.querySelector("#vid1")
  
  closeWebCambutton.addEventListener('click', function() {
    startButton.innerHTML =" ";
    startButton.innerHTML +=`<div id="custom-container"%"><a href="/" style="text-decoration:none !important;
    color: #000;">Restart Session</a></div>`
    video.style.display=('none')
  })
}