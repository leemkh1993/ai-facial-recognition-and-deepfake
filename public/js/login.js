function openForm() {
  document.getElementById("loginForm").style.display = "block";
}

function closeForm() {
  document.getElementById("loginForm").style.display = "none";
}

//  20/8 login logic by LC
console.log("u loaded login.js")
document.querySelector('#loginloginForm') //class instead of id
    .addEventListener('submit', async function(event){
        console.log("u click login btn");
        event.preventDefault();
        const form = event.target;// if consolelog will browse the whole "form" element
        const formObject = {};

        formObject.username = form.email.value; //?match to html name or database name?
        formObject.password = form.psw.value;

        const res = await fetch('/login',{
            method:"POST",
            headers:{
               "Content-Type":"application/json"
            },
            body: JSON.stringify(formObject),
        });
        const result = await res.json();
        // console.log(res.json()); res.json()只可以出現一次


        if(res.status === 200 && result.Success){ 

          console.log(formObject['username']);
          closeForm();
          localStorage.setItem("username",formObject['username']);

          // const signInForm = document.querySelector("#loginloginForm")
          // signInForm.action = "/logout";
          // signInForm.method = "DELETE"
        
          //create logout btn
          const logoutBtn = document.createElement("a");
            logoutBtn.innerHTML='Logout';
            logoutBtn.className="logout";
            logoutBtn.href="/logout";
                  
          // 21/8 change content of "signInNow"btn by LC  (md:C)
          const signined = document.getElementById("signInContainer");
            // signined.innerHTML=`Hi! ${form.email.value}  `;
            signined.id="signInContainer";
            signined.onclick = null;

            signined.prepend(logoutBtn);
            document.getElementById('signInNow').classList.add('active')
            document.getElementById('signInNow').style.display=('none')

            $('#loginloginForm').attr("action","");

            // document.getElementById('signInNow').appendChild(logoutBtn);
            // $("#loginloginForm").attr("action","");


             // 22/8 name of the USER in the Gallery (md:C)
            document.getElementById("deepMediaUser").innerHTML ="";
            document.getElementById("deepMediaUser").innerHTML +=`
            <h3 id="deepMediaUser"><i class="fas fa-user"></i> ${form.email.value}'s GALLERY</h3>`
            document.querySelector('#clearAll').classList.remove('d-none');
            // <h3 id="deepMediaUser"><i class="fas fa-users"></i>USER GALLERY</h3>

          // 21/8 show username to <div id="container"> (md:C)
            document.querySelector('#container').classList.remove('d-none');
            document.querySelector('#you-are-user').innerHTML = `${result.firstName.charAt(0)+""+result.lastName.charAt(0)}`;
            document.querySelector('#name').innerHTML = `${result.firstName.charAt(0)+""+result.lastName.charAt(0)}`;
            
            // 22/8 color theme of buttons (md:C)
            document.querySelector('#capture').classList.add('active');
            document.querySelector('#btnStop').classList.add('active');

            // 23/8 login button > message-textarea-submittion (md:C)
            document.querySelector('#create-messages-form').classList.add('active');
            
            // 23/8 allow to chat after login
            document.querySelector('#create-messages-form').innerHTML=""
            document.querySelector('#create-messages-form').innerHTML +=`
            <form id="create-messages-form" action="/submitMessage" method="POST">
              <div id="newMessagesContainer">
                <div id="deepMediaUsers-container">  
                  <div id="you-are-user" name="dm">${result.firstName.charAt(0)+""+result.lastName.charAt(0)}</div>
                </div>
                  <textarea id="input-text" class="send-messages-input" placeholder="Say something..." name="content"></textarea>
                  <input type="submit" class="btn btn-primary send-messages-button" value="Send">
                </div>
                <p class="varchar">0/200&nbsp;<i class="far fa-paper-plane"></i></p>
              </div>
            </form>`

             // 23/8 allow to clear items in the Gallery (md:C)
              function clearAllinGallery() {
                let clearAllItems = document.querySelector('#clearAll')
                let newSideBar = document.querySelector('#mySidebar')
              
              // 23/8 allow to clear items in the Gallery (md:C) will explain why put here
              
             clearAllItems.addEventListener('click', function() {
               newSideBar.innerHTML = "";
               newSideBar.innerHTML += `<div>
               <h3 id="deepMediaUser"><i class="fas fa-user"></i> ${result.firstName}'s GALLERY</h3><hr>
               <button id="clearAll" class="btn btn-secordary">Clear All</button>
               <div><span id="closetimers" class="closebtn" onclick="closeNav()">&times;</span></div>
               <div class="captures-container"><img id="screencap" class="image-fluid src="https://i.pinimg.com/236x/c2/69/2a/c2692ad105e0ba68804cbcc9536cf89e.jpg" alt"photo-of-the-host"></div>
               <div class="video-recording-container"><video id="vid2"></video></div>`
             })
           };
           clearAllinGallery()
              
            // unlock all the disable btns for user (md:C)
            $('#btnStart').prop("disabled", false);
            $('#btnStop').prop("disabled", false);
            $('#capture').prop("disabled", false);
            $('#audio-switch').prop("disabled", false);
            $('.send-messages-button').prop("disabled", false);


            /*========================== Get user comment responses logic draws here for the sake of getting the username ==========================*/
            const messageForm = document.querySelector('#create-messages-form') 

            messageForm.addEventListener('submit', async function(event) {
            event.preventDefault();
           
            const form = event.target;
            
            const formObj = {}
            formObj.content = form.content.value;
        
            console.log(form.content.value)
            // console.log(form.res.body)
            console.log("message sent!")
        
            const res = await fetch('/submitMessage',{
        
                method:"POST", 
                headers:{
                    "Content-Type":"application/json"
                },
                body: JSON.stringify(formObj)
                });
                const result = await res.json();
                console.log(result)
                form.reset();
                if(res.status === 200 && result.success){
                // form.reset();
                } else if (res.status === 401){
                console.log("failed to fetch meesage, please check server route");
                } 
                getMessaes();
        
            });

            async function getMessaes(){
              // const newMessages = Array.form(document.querySelectorAll('.comments'));
              const res = await fetch('/getMessage');
              const messages = await res.json();
              const now = new Date();
              const today = `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`;
            
              const scrollyContainer = document.querySelector('#scrolly-messages-container')
              setTimeout(function(){
              scrollyContainer.innerHTML = "";
              
              // const firstMessage = message[0]
              
              for(let message of messages){
                  console.log(message)
                  // user_id = 1,2,3,4,5

              scrollyContainer.innerHTML +=`
              <div class="comments">
                  <div class="username">${result.firstName.charAt(0)+""+result.lastName.charAt(0)}</div>
                      <div class="status"><span class="cm-areas"> ${result.firstName} ${today} min ago
                          <p class="cm-text"> ${message.content} </p></span>
                      </div>
              </div>
              </div>`;
              };
              },100);
              
              // whem newFeed is added, arrow shows up
              setTimeout(function(){
              document.querySelector('#altArrow').addEventListener('click', function(){
              document.querySelector('#altArrow').style.display = ('show')    
              })
            
              },1200);
            }

          /*=============== if login is failed || not a res.session => redirect to home Page ==================*/


          } else if (res.status === 401 ||res.status === 404) {
            console.log("login failed");
            document.querySelector('#loginFail-message').classList.remove('d-none');
            setTimeout(function(){
              document.querySelector('#loginFail-message').classList.add('d-none');
            },2500);
          };
});

// forever logs -f 0
// cat path?


